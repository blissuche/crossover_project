using System.Collections.Generic;

namespace crossblog.Model
{
    public class ArticleListModel
    {
        public IEnumerable<ArticleModel> Articles { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}